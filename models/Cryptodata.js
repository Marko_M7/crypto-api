const mongoose = require("mongoose")
const Schema = mongoose.Schema;

//Modèle base de données
//Complet
const CryptoValue = new Schema({
    _id : String,
    uuid: String,
    symbol: String,
    name: String,
    iconUrl : String,
    chart :[{ price: Number, timestamp : String,}],
    versionKey: false
});

 const CryptoValueModel = mongoose.model("Cryptodatas", CryptoValue)
 module.exports = {CryptoValueModel};

