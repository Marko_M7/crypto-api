const express = require("express");
const app = express();

const routeCrypto = require("./routes/server");
const routeCoinsss = require("./routes/serverBis")

const fetch = require("cross-fetch");
const mongoose = require("mongoose");

const bodyParser = require("body-parser");
const cors = require("cors")

app.use(cors());
app.use(bodyParser.json());

// Connexion to MongoDb Atlas
const uri = "mongodb+srv://marko:rg1GfVLr1lVjyGsN@cryptoapi.s3qho.mongodb.net/myFirstDatabase";
mongoose.connect(uri, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  })
  .then(() => console.log(`connection DB réussie`));

//connection au port 4000
var porte = process.env.PORT || 4000;
var server =app.listen(porte,function() {
  console.log("app running on port " + porte); 
});


//Middleware
app.use("/crypto", routeCrypto);
app.use("/coins", routeCoinsss);

app.get("/", function (req, res) {
  res.send("Bienvenue sur la plateforme Cryptonite");
});


