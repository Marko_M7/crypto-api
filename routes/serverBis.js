const express = require("express");
const router = express.Router();

const modelCryptos = require("../models/Cryptodata").CryptoValueModel;

// Recuperer toutes les crypto
router.get("/getsCoins", function (req, res) {
    modelCryptos.find({}, (error, posts) => {
      if (error) {
        res.status(400).json({error:error});
        return;
      }
      res.status(200).send({
        response: posts,
      });
    });
});

// Recuperer crypto par symbole
router.get("/getsCoinChart/:symbol", (req, res) => {
    const symbol = req.params.symbol;
     modelCryptos.find({ symbol: symbol }, 'symbol chart', (error, response) => {
      if (error) {
        res.status(400).json({ message: "Erreur, symbole indisponible"});
        return;
      }
      res.status(200).send({ response})
    });
  });

module.exports = router;